# Development

## Architecture

The following diagram describes how the `release:` keyword in `.gitlab-ci.yml` is processed:

```mermaid
sequenceDiagram
  participant Rails
  participant ReleaseCLI
  participant Runner
  Runner->>Rails: 1. Runner calls API for job info
  Rails->>Rails : 2. Yaml exposed as Steps
  Runner->>ReleaseCLI : 3. Runner calls CLI on Job success
  ReleaseCLI->>Rails : 4. CLI retrieves Release Steps
  ReleaseCLI->>Rails : 5. CLI creates Release
```

1. Runner calls API for job info: the runner polls Rails for new jobs.

1. Yaml exposed as **Steps**: The `release` node of the `.gitlab-ci.yml` configuration is converted into **Steps** and made available via the API endpoint.

1. Runner calls CLI on job success: the runner executes the job, and upon success calls the **GitLab Release**.

1. CLI retrieves Release **Steps**: the GitLab Release CLI calls the Rails API to retrieve the `release` configuration (as **Steps**).

1. CLI creates Release: the GitLab Release CLI makes an API call to Rails to create the new Release.

## Video demo

[This video](https://www.youtube.com/watch?v=82MNMx5wJCQ) shows how to run and test `release-cli` locally and in docker.
